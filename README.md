# eX' CS:GO Config

Based upon my CS:Source config, of which the original README is below:


Over the years that I've been playing CSS, I've tweaked a lot of aspects of the game to my liking.

Even though nowadays I do not play CSS all that often, I still keep my configuration around.

Feel free to use (parts of) my config, just don't suck as much as I do.

Thanks to Valve for this addicting game on the best PC game engine there is, Source.

## Installing and using my config

Installing is easy, just git clone or download as a zip, and unpack in your cstrike/cfg dir.

When ingame, open up your console and type the command 'exec ex_autoexec' to load my config.

Alternatively, add the command to your autoexec.cfg to autoload my config upon starting the game.

## In-game help

This config features an in-game help function, just press F1 to view it.
